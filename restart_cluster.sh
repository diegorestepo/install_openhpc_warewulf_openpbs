#!/bin/bash


# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------


sms_ip=192.168.1.254
internal_netmask=255.255.255.0
sms_eth_internal=enp0s3


/usr/sbin/ip link set dev ${sms_eth_internal} up
/usr/sbin/ip address add ${sms_ip}/${internal_netmask} broadcast + dev ${sms_eth_internal}

systemctl restart dhcpd


exit 0
