# Install OpenHPC Warewulf OpenPBS

Script for install OpenHPC Warewulf OpenPBS in CentOS 8.4 (x86 64).

<b>Note:</b> This script is for one master and one computer node only.


## Step 1:
Change the value of the variables according to your system:

>>>
###### Master variables:
sms_name=master <br>
sms_ip=192.168.1.254 <br>
internal_netmask=255.255.255.0 <br>
sms_eth_internal=enp0s3 <br>

##### Computer
eth_provision=enp0s3 <br>
c_name=c1 <br>
c_ip=192.168.1.253 <br>
c_mac=08:00:27:3C:0B:06 <br>
kargs=quiet <br>
compute_regex=${c_name} <br>

##### Nagios
nagios_web_password=123456 <br>
>>>


## Step 2:
Grant execute permissions to script:
>>>
chmod u+xr install_openHPC_OpenPBS_CentOS84.sh
>>>


## Step 3:
### Run script:
root:
>>>
./install_openHPC_OpenPBS_CentOS84.sh
>>>


## Restart the cluster:
Making use of cron (crontab -e):
>>>
@reboot bash /home/you/directory/restart_cluster.sh
>>>


## References:
- OpenHPC: https://github.com/openhpc/ohpc
- OpenHPC 2.x: https://github.com/openhpc/ohpc/wiki/2.x
- OpenHPC (v2.3) Cluster Building Recipes: https://github.com/openhpc/ohpc/releases/download/v2.3.GA/Install_guide-CentOS8-Warewulf-OpenPBS-2.3-x86_64.pdf
- How to Install OpenHPC Slurm (part 1/3)(2018): https://www.youtube.com/watch?v=7Am1WAqQl7M&ab_channel=AtipPeethong
- How to Install OpenHPC Slurm (part 2/3)(2018): https://www.youtube.com/watch?v=UfWeZ6k0KXM&ab_channel=AtipPeethong
- How to Install OpenHPC Slurm (part 3/3)(2018): https://www.youtube.com/watch?v=EwbSBq23RRk&ab_channel=AtipPeethong
