#!/bin/bash


# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------


clear


AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


echo
echo "$VERDE=================================================$LIMPIAR"
echo "$VERDE=                    START                      =$LIMPIAR"
echo "$VERDE=================================================$LIMPIAR"
echo

# Master
sms_name=master
sms_ip=192.168.1.254
internal_netmask=255.255.255.0
sms_eth_internal=enp0s3

# Computer
eth_provision=enp0s3
c_name=c1
c_ip=192.168.1.253
c_mac=08:00:27:3C:0B:06
kargs=quiet
compute_regex=${c_name}

# Nagios
nagios_web_password=123456


echo
echo "$AZUL 1. Update, install WGET and tmux $LIMPIAR"
echo
yum -y update
yum install -y wget
yum install -y tmux


echo
echo "$AZUL 2. Install Base Operating System (BOS) $LIMPIAR"
echo
echo ${sms_ip}    ${sms_name} >> /etc/hosts
systemctl disable firewalld
systemctl stop firewalld


echo
echo "$AZUL 3. Install OpenHPC Components $LIMPIAR"
echo
echo "$LILA 3.1 Enable OpenHPC repository for local use $LIMPIAR"
echo
yum install -y http://repos.openhpc.community/OpenHPC/2/CentOS_8/x86_64/ohpc-release-2-1.el8.x86_64.rpm
yum install -y dnf-plugins-core
yum config-manager --set-enabled powertools


echo
echo "$LILA 3.3 Add provisioning services on master node $LIMPIAR"
echo
# Install base meta-packages
yum -y install ohpc-base
yum -y install ohpc-warewulf

systemctl enable chronyd.service
echo "server 1.co.pool.ntp.org" >> /etc/chrony.conf
echo "server 3.south-america.pool.ntp.org" >> /etc/chrony.conf
echo "server 0.south-america.pool.ntp.org" >> /etc/chrony.conf
echo "allow all" >> /etc/chrony.conf
systemctl restart chronyd


echo
echo "$LILA 3.4 Add resource management services on master node $LIMPIAR"
echo
yum -y install openpbs-server-ohpc


echo
echo "$LILA 3.7 Complete basic Warewulf setup for master node $LIMPIAR"
echo
# Configure Warewulf provisioning to use desired internal interface
perl -pi -e "s/device = eth1/device = ${sms_eth_internal}/" /etc/warewulf/provision.conf

# Enable internal interface for provisioning
ip link set dev ${sms_eth_internal} up
ip address add ${sms_ip}/${internal_netmask} broadcast + dev ${sms_eth_internal}

# Restart/enable relevant services to support provisioning
systemctl enable httpd.service
systemctl restart httpd
systemctl enable dhcpd.service
systemctl enable tftp.socket
systemctl start tftp.socket


echo
echo "$LILA 3.8 Define compute image for provisioning $LIMPIAR"
echo
echo "$LILA 3.8.1 Build initial BOS image $LIMPIAR"
echo
# Define chroot location
export CHROOT=/opt/ohpc/admin/images/centos8.4

# Build initial chroot image
wwmkchroot -v centos-8 $CHROOT

# Enable OpenHPC and EPEL repos inside chroot
dnf -y --installroot $CHROOT install epel-release
cp -p /etc/yum.repos.d/OpenHPC*.repo $CHROOT/etc/yum.repos.d


echo
echo "$LILA 3.8.2 Add OpenHPC components $LIMPIAR"
echo
# Install compute node base meta-package
yum -y --installroot=$CHROOT install ohpc-base-compute

cp -p /etc/resolv.conf $CHROOT/etc/resolv.conf

# Add OpenPBS client support
yum -y --installroot=$CHROOT install openpbs-execution-ohpc
perl -pi -e "s/PBS_SERVER=\S+/PBS_SERVER=${sms_name}/" $CHROOT/etc/pbs.conf
echo "PBS_LEAF_NAME=${sms_name}" >> /etc/pbs.conf
chroot $CHROOT opt/pbs/libexec/pbs_habitat
perl -pi -e "s/\$clienthost \S+/\$clienthost ${sms_name}/" $CHROOT/var/spool/pbs/mom_priv/config
echo "\$usecp *:/home /home" >> $CHROOT/var/spool/pbs/mom_priv/config
chroot $CHROOT systemctl enable pbs

# Add Network Time Protocol (NTP) support
yum -y --installroot=$CHROOT install chrony

# Identify master host as local NTP server
echo "server    ${sms_ip}" >> $CHROOT/etc/chrony.conf

# Add kernel drivers (matching kernel version on SMS node)
yum -y --installroot=$CHROOT install kernel-`uname -r`

# Include modules user environment
yum -y --installroot=$CHROOT install lmod-ohpc


echo
echo "$LILA 3.8.3 Customize system configuration $LIMPIAR"
echo
# Initialize warewulf database and ssh_keys
wwinit database
wwinit ssh_keys

# Add NFS client mounts of /home and /opt/ohpc/pub to base image
echo "${sms_ip}:/home /home nfs nfsvers=3,nodev,nosuid 0 0" >> $CHROOT/etc/fstab
echo "${sms_ip}:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev 0 0" >> $CHROOT/etc/fstab

# Export /home and OpenHPC public packages from master server
echo "/home *(rw,no_subtree_check,fsid=10,no_root_squash)" >> /etc/exports
echo "/opt/ohpc/pub *(ro,no_subtree_check,fsid=11)" >> /etc/exports
exportfs -a
systemctl restart nfs-server
systemctl enable nfs-server


echo
echo "$LILA 3.8.4.6 Enable forwarding of system logs $LIMPIAR"
echo
# Configure SMS to receive messages and reload rsyslog configuration
echo "module(load="imudp")" >> /etc/rsyslog.d/ohpc.conf
echo "input(type="imudp" port="514")" >> /etc/rsyslog.d/ohpc.conf
systemctl restart rsyslog

# Define compute node forwarding destination
echo "*.* @${sms_ip}:514" >> $CHROOT/etc/rsyslog.conf
echo "Target=\"${sms_ip}\" Protocol=\"udp\"" >> $CHROOT/etc/rsyslog.conf

# Disable most local logging on computes. Emergency and boot logs will remain on the compute nodes
perl -pi -e "s/^\*\.info/\\#\*\.info/" $CHROOT/etc/rsyslog.conf
perl -pi -e "s/^authpriv/\\#authpriv/" $CHROOT/etc/rsyslog.conf
perl -pi -e "s/^mail/\\#mail/" $CHROOT/etc/rsyslog.conf
perl -pi -e "s/^cron/\\#cron/" $CHROOT/etc/rsyslog.conf
perl -pi -e "s/^uucp/\\#uucp/" $CHROOT/etc/rsyslog.conf


echo
echo "$LILA 3.8.4.7 Add Nagios monitoring $LIMPIAR"
echo
# Install nagios, nrep, and all available plugins on master host
yum -y install --skip-broken nagios nrpe nagios-plugins-*

# Install nrpe and an example plugin into compute node image
yum -y --installroot=$CHROOT install nrpe nagios-plugins-ssh

# Enable and configure Nagios NRPE daemon in compute image
chroot $CHROOT systemctl enable nrpe
perl -pi -e "s/^allowed_hosts=/# allowed_hosts=/" $CHROOT/etc/nagios/nrpe.cfg
echo "nrpe : ${sms_ip} : ALLOW" >> $CHROOT/etc/hosts.allow
echo "nrpe : ALL : DENY" >> $CHROOT/etc/hosts.allow

# Copy example Nagios config file to define a compute group and ssh check
# (note: edit as desired to add all desired compute hosts)
cp /opt/ohpc/pub/examples/nagios/compute.cfg /etc/nagios/objects

# Register the config file with nagios
echo "cfg_file=/etc/nagios/objects/compute.cfg" >> /etc/nagios/nagios.cfg

# Update location of mail binary for alert commands
perl -pi -e "s/ \/bin\/mail/ \/usr\/bin\/mailx/g" /etc/nagios/objects/commands.cfg

# Update email address of contact for alerts
perl -pi -e "s/nagios\@localhost/root\@${sms_name}/" /etc/nagios/objects/contacts.cfg

# Add check_ssh command for remote hosts
echo command[check_ssh]=/usr/lib64/nagios/plugins/check_ssh localhost $CHROOT/etc/nagios/nrpe.cfg

# define password for nagiosadmin to be able to connect to web interface
htpasswd -bc /etc/nagios/passwd nagiosadmin ${nagios_web_password}

# Enable Nagios on master, and configure
systemctl enable nagios
systemctl start nagios

# Update permissions on ping command to allow nagios user to execute
chmod u+s `which ping`


echo
echo "$LILA 3.8.4.10 Add Magpie $LIMPIAR"
echo
yum -y install magpie-ohpc


echo
echo "$LILA 3.8.5 Import files $LIMPIAR"
echo
wwsh file import /etc/passwd
wwsh file import /etc/group
wwsh file import /etc/shadow


echo
echo "$LILA 3.9 Finalizing provisioning configuration $LIMPIAR"
echo
echo "$LILA 3.9.1 Assemble bootstrap image $LIMPIAR"
echo
export WW_CONF=/etc/warewulf/bootstrap.conf
echo "drivers += updates/kernel/" >> $WW_CONF
wwbootstrap `uname -r`


echo
echo "$LILA 3.9.2 Assemble Virtual Node File System (VNFS) image $LIMPIAR"
echo
wwvnfs --chroot $CHROOT


echo
echo "$LILA 3.9.3 Register nodes for provisioning $LIMPIAR"
echo
# Set provisioning interface as the default networking device
echo "GATEWAYDEV=${eth_provision}" > /tmp/network.$$
wwsh -y file import /tmp/network.$$ --name network
wwsh -y file set network --path /etc/sysconfig/network --mode=0644 --uid=0

# Add nodes to Warewulf data store
wwsh -y node new ${c_name} --ipaddr=${c_ip} --hwaddr=${c_mac} -D ${eth_provision}

# Additional step required if desiring to use predictable network interface
# naming schemes (e.g. en4s0f0). Skip if using eth# style names.
export kargs="${kargs} net.ifnames=1,biosdevname=1"
wwsh provision set --postnetdown=1 "${compute_regex}"

# Define provisioning image for hosts
wwsh -y provision set "${compute_regex}" --vnfs=centos8.4 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,network

# Restart dhcp / update PXE
systemctl restart dhcpd
wwsh pxe update


echo
echo "$VERDE=================================================$LIMPIAR"
echo "$VERDE=             TURN ON THE COMPUTER              =$LIMPIAR"
echo "$VERDE=================================================$LIMPIAR"
echo
read -p "Press [Enter] when CentOS has started..."


echo
echo "$LILA 3.10 Boot compute nodes $LIMPIAR"
echo
pdsh -w c1 uptime


echo
echo "$AZUL 4. Install OpenHPC Development Components $LIMPIAR"
echo
echo "$LILA 4.1 Development Tools $LIMPIAR"
echo
# Install autotools meta-package
yum -y install ohpc-autotools
yum -y install EasyBuild-ohpc
yum -y install hwloc-ohpc
yum -y install spack-ohpc
yum -y install valgrind-ohpc


echo
echo "$LILA 4.2 Compilers $LIMPIAR"
echo
yum -y install gnu9-compilers-ohpc


echo
echo "$LILA 4.3 MPI Stacks $LIMPIAR"
echo
yum -y install openmpi4-gnu9-ohpc mpich-ofi-gnu9-ohpc


echo
echo "$LILA 4.4 Performance Tools $LIMPIAR"
echo
yum -y install ohpc-gnu9-perf-tools


echo
echo "$LILA 4.5 Setup default development environment $LIMPIAR"
echo
yum -y install lmod-defaults-gnu9-openmpi4-ohpc


echo
echo "$LILA 4.6 3rd Party Libraries and Tools $LIMPIAR"
echo
# Install 3rd party libraries/tools meta-packages built with GNU toolchain
yum -y install ohpc-gnu9-serial-libs
yum -y install ohpc-gnu9-io-libs
yum -y install ohpc-gnu9-python-libs
yum -y install ohpc-gnu9-runtimes

# Install parallel lib meta-packages for all available MPI toolchains
yum -y install ohpc-gnu9-mpich-parallel-libs
yum -y install ohpc-gnu9-openmpi4-parallel-libs


echo
echo "$AZUL 5. Resource Manager Startup $LIMPIAR"
echo
# start PBS daemons on master host
systemctl enable pbs
systemctl start pbs

# initialize PBS path
. /etc/profile.d/pbs.sh

# enable user environment propagation (needed for modules support)
qmgr -c "set server default_qsub_arguments= -V"

# enable uniform multi-node MPI task distribution
qmgr -c "set server resources_default.place=scatter"

# enable support for job accounting
qmgr -c "set server job_history_enable=True"

# register compute hosts with PBS
qmgr -c "create node ${c_name}"


echo
echo "$VERDE=================================================$LIMPIAR"
echo "$VERDE=                      END                      =$LIMPIAR"
echo "$VERDE=================================================$LIMPIAR"
echo


exit 0
